/* Copyright: Agence universitaire de la Francophonie - www.auf.org
 * License: GNU Public License version 3
 * Author: Jean Christophe André
 * Inspired from http://www.webdesignerdepot.com/2012/01/password-strength-verification-with-jquery/
 */

function valid_pass1_criteria() {
    var p = $('#id_new_password1').val()
    return (p.length >= 8) && p.match(/[a-z]/) && p.match(/[A-Z]/) && p.match(/[0-9]/) && p.match(/[^a-zA-Z0-9]/) && !p.match(/[?*\"]/);
}

function valid_pass2_equals_pass1() {
    var p = $('#id_new_password2').val()
    var valid = ( (p != '') && (p == $('#id_new_password1').val()) );
    $('label[for=id_new_password2]').toggleClass('input-valid', valid);
    return valid;
}

$(document).ready(function() {
    // bouton submit caché au départ
    $('input[type=submit]').hide();

    // validation du mot de passe
    $('#id_new_password1').bind('keyup focus blur', function() {
        var p = $(this).val();
        var valid = true;
        var valid2;
        // longueur suffisante
        if (p.length >= 8) {
            $('#pw-critere-length').removeClass('item-invalid').addClass('item-valid');
        } else {
            $('#pw-critere-length').removeClass('item-valid').addClass('item-invalid');
            valid = false;
        }
        // présence d'une minuscule
        if (p.match(/[a-z]/)) {
            $('#pw-critere-lower').removeClass('item-invalid').addClass('item-valid');
        } else {
            $('#pw-critere-lower').removeClass('item-valid').addClass('item-invalid');
            valid = false;
        }
        // présence d'une majuscule
        if (p.match(/[A-Z]/)) {
            $('#pw-critere-upper').removeClass('item-invalid').addClass('item-valid');
        } else {
            $('#pw-critere-upper').removeClass('item-valid').addClass('item-invalid');
            valid = false;
        }
        // présence d'un chiffre
        if (p.match(/[0-9]/)) {
            $('#pw-critere-digit').removeClass('item-invalid').addClass('item-valid');
        } else {
            $('#pw-critere-digit').removeClass('item-valid').addClass('item-invalid');
            valid = false;
        }
        // présence d'un caractère spécial
        if (p.match(/[^a-zA-Z0-9]/)) {
            $('#pw-critere-special').removeClass('item-invalid').addClass('item-valid');
        } else {
            $('#pw-critere-special').removeClass('item-valid').addClass('item-invalid');
            valid = false;
        }
        // absence de caractère invalide
        if (p.match(/[?*\"]/)) {
            $('#pw-critere-forbidden').removeClass('item-valid').addClass('item-invalid');
            valid = false;
        } else {
            $('#pw-critere-forbidden').removeClass('item-invalid').addClass('item-valid');
        }
        // validation du champ complet
        $('label[for=id_new_password1]').toggleClass('input-valid', valid);
        // validation de l'égalité du second mot de passe
        valid2 = valid_pass2_equals_pass1();
        // affichage conditionnel du bouton submit
        $('input[type=submit]').toggle(valid && valid2);
    });

    // validation du second mot de passe et gestion du bouton submit
    $('#id_new_password2').bind('keyup focus blur', function() {
        valid = valid_pass1_criteria();
        // validation de l'égalité du second mot de passe
        valid2 = valid_pass2_equals_pass1();
        // affichage conditionnel du bouton submit
        $('input[type=submit]').toggle(valid && valid2);
    });
});
