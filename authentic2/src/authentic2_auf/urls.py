from django.conf.urls import patterns, url

from authentic2.decorators import setting_enabled, required

from . import app_settings
from .views import lost_password_login, secret_questions, lost_password_reset

urlpatterns = required(
        setting_enabled('ENABLE', settings=app_settings),
        patterns('',
            url('^accounts/auf/lost-password-login/$', lost_password_login,
                name='authentic2-auf-lost-password-login'),
            url('^accounts/auf/secret-questions/$', secret_questions,
                name='authentic2-auf-secret-questions'),
            # remplace la vue Django de recuperation
            url('^accounts/password/reset/$', lost_password_reset,
                name='auth_password_reset'),
        )
)
